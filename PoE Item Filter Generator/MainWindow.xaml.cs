﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PoE_Item_Filter_Generator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        String lootFilter;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            foreach (CheckBox c in Rarities.Children)
            {
                if ((c.GetType() == typeof(CheckBox)) && c.IsChecked.Value == true)
                    c.IsChecked = false;
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            createFilter();
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Put your filter in your my games/path of exile directory";
            saveFileDialog.Filter = "Filter file (*.filter)|*.filter";
            if (saveFileDialog.ShowDialog() == true)
                File.WriteAllText(saveFileDialog.FileName, lootFilter);
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void createFilter()
        {
            if (ShowMagic.IsChecked.Value)
            {
                lootFilter += "Show\n";
                lootFilter += "\tRarity Magic\n";
            }

            if (ShowRare.IsChecked.Value)
            {
                lootFilter += "Show\n";
                lootFilter += "\tRarity Rare\n";
            }
           
            if (ShowUnique.IsChecked.Value)
            {
                lootFilter += "Show\n";
                lootFilter += "\tRarity Unique\n";
            }

        }

        
    }
}
/*        <GroupBox Header="Border Color" HorizontalAlignment="Left" VerticalAlignment="Top" Grid.Row="1" Height="236" Width="263" Margin="273,0,0,0">
            <Grid Name="BorderColor">
                <Slider Name="" Content="Dim" HorizontalAlignment="Left" Margin="10,20,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
                <RadioButton Name="Hide" Content="Hide" HorizontalAlignment="Left" Margin="10,40,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
            </Grid>
        </GroupBox>
        <GroupBox Header="Background Color" HorizontalAlignment="Left" VerticalAlignment="Top" Grid.Row="1" Height="236" Width="263" Margin="273,0,0,0">
            <Grid Name="Junk">
                <RadioButton Name="Dim" Content="Dim" HorizontalAlignment="Left" Margin="10,20,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
                <RadioButton Name="Hide" Content="Hide" HorizontalAlignment="Left" Margin="10,40,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
            </Grid>
        </GroupBox>
        <GroupBox Header="Text Color" HorizontalAlignment="Left" VerticalAlignment="Top" Grid.Row="1" Height="236" Width="263" Margin="273,0,0,0">
            <Grid Name="Junk">
                <RadioButton Name="Dim" Content="Dim" HorizontalAlignment="Left" Margin="10,20,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
                <RadioButton Name="Hide" Content="Hide" HorizontalAlignment="Left" Margin="10,40,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
                <RadioButton Name="Filter by item level" Content="Hide" HorizontalAlignment="Left" Margin="10,40,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
            </Grid>
        </GroupBox>
        <GroupBox Header="Alert Sound" HorizontalAlignment="Left" VerticalAlignment="Top" Grid.Row="1" Height="236" Width="263" Margin="273,0,0,0">
            <Grid Name="Junk">
                <RadioButton Name="Dim" Content="Dim" HorizontalAlignment="Left" Margin="10,20,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
                <RadioButton Name="Hide" Content="Hide" HorizontalAlignment="Left" Margin="10,40,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
            </Grid>
        </GroupBox>
        <GroupBox Header="Jewelry" HorizontalAlignment="Left" VerticalAlignment="Top" Grid.Row="1" Height="236" Width="263" Margin="273,0,0,0">
            <Grid Name="Junk">
                <RadioButton Name="Dim" Content="Dim" HorizontalAlignment="Left" Margin="10,20,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
                <RadioButton Name="Hide" Content="Hide" HorizontalAlignment="Left" Margin="10,40,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
                <RadioButton Name="Filter by item level" Content="Hide" HorizontalAlignment="Left" Margin="10,40,0,0" VerticalAlignment="Top" Height="15" Width="96"/>
            </Grid>
        </GroupBox>
*/